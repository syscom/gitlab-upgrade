#!/bin/bash
export SKIP_POST_DEPLOYMENT_MIGRATIONS=true

tmp=$(getopt -o D --long with-downtime -n "$0" -- "$@")

[ $? -ne 0 ] && exit 1

eval set -- "$tmp"

while true; do
  case "$1" in
    -D|--with-downtime)
      unset SKIP_POST_DEPLOYMENT_MIGRATIONS
      shift
      continue
      ;;
    --)
      shift
      break
      ;;
  esac
done

[ $# -ne 1 ] && echo "Usage: $0 [git tree-ish]" && exit 1

set -xe

cd /home/git/gitlab
oldrev="$(git describe --abbrev=0)"

git fetch --all --tags

git checkout -- db/schema.rb
git rebase --onto "$1" "$oldrev" || { echo "Merge conflict, starting shell so you can fix it" && bash || true; }

cd /home/git/gitlab-shell
git fetch --all --tags
git checkout "v$(</home/git/gitlab/GITLAB_SHELL_VERSION)"
make build

cd /home/git/gitlab-workhorse
git fetch --all --tags
git checkout "v$(</home/git/gitlab/GITLAB_WORKHORSE_VERSION)"
make

cd /home/git/gitaly
git fetch --all --tags
git checkout -- Makefile
git checkout "v$(</home/git/gitlab/GITALY_SERVER_VERSION)"
sed -ri 's/^\tcp \$/\tcp -f \$/' Makefile # Allow copying while running
# Make sure pkg-config is installed so nokogiri can be compiled with system libraries
GEM_HOME=$PWD/ruby/vendor/bundle/$(ruby -e "puts RUBY_ENGINE + '/' + RbConfig::CONFIG['ruby_version']") gem install pkg-config
make

cd /home/git/gitlab-pages
git fetch --all --tags
git checkout "v$(</home/git/gitlab/GITLAB_PAGES_VERSION)"
make CGO_ENABLED=1

cd /home/git/gitlab
# Make sure pkg-config is installed so nokogiri can be compiled with system libraries
GEM_HOME=$PWD/vendor/bundle/$(ruby -e "puts RUBY_ENGINE + '/' + RbConfig::CONFIG['ruby_version']") gem install pkg-config
bundle install --without mysql development test kerberos --deployment
bundle clean
bundle exec rake db:migrate RAILS_ENV=production
bundle exec rake gettext:compile RAILS_ENV=production
NODE_OPTIONS=--max-old-space-size=4096 bundle exec rake yarn:install gitlab:assets:clean gitlab:assets:compile RAILS_ENV=production NODE_ENV=production
bundle exec rake cache:clear RAILS_ENV=production

# check for gitlab.yml updates
changed="$(git diff-index --cached "$oldrev" config/gitlab.yml.example)"
if [ "$changed" ]; then
  echo "New config in gitlab.yml, trying to merge automatically..."
  if ! git diff "$oldrev" config/gitlab.yml.example | sed 's/\.example//g' | patch -p1 --merge=diff3; then
    echo "Merge conflicts, starting shell so you can resolve them."
    bash || true
   fi
fi

# check for nginx config updates
changed="$(git diff-index --cached "$oldrev" lib/support/nginx/gitlab-ssl)"
[ "$changed" ] && echo "New nginx config for gitlab, starting shell so you can merge it" && bash || true
changed="$(git diff-index --cached "$oldrev" lib/support/nginx/gitlab-pages-ssl)"
[ "$changed" ] && echo "New nginx config for pages, starting shell so you can merge it" && bash || true
