#!/bin/bash

tmp=$(getopt -o Ds --long with-downtime,skip-checks -n "$0" -- "$@")

[ $? -ne 0 ] && exit 1

eval set -- "$tmp"

while true; do
  case "$1" in
    -D|--with-downtime)
      downtime=downtime
      shift
      continue
      ;;
    -s|--skip-checks)
      skip_checks=skip_checks
      shift
      continue
      ;;
    --)
      shift
      break
      ;;
  esac
done

[ "$#" -ne 1 ] && echo "Usage: $0 [git tree-ish]" && exit 1

set -xe

[ "$downtime" ] && systemctl stop gitlab-unicorn gitlab-workhorse gitlab-gitaly gitlab-sidekiq gitlab-pages

sudo -u git -H ./upgrade.sh ${downtime:+--with-downtime} "$@"

systemctl restart gitlab-workhorse gitlab-gitaly gitlab-sidekiq gitlab-pages
systemctl reload nginx

if [ "$downtime" ]; then
  systemctl restart gitlab-unicorn
else
  systemctl reload gitlab-unicorn
  (cd /home/git/gitlab; sudo -u git -H bundle exec rake db:migrate RAILS_ENV=production)
fi

[ "$skip_checks" ] && exit 0

cd /home/git/gitlab
sudo -u git -H bundle exec rake gitlab:env:info RAILS_ENV=production | less -R
sudo -u git -H bundle exec rake gitlab:check RAILS_ENV=production | less -R
